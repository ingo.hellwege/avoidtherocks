/* init */
var run=false;                    // game status
var brk=false;                    // game pause
var winw=0;                       // screen width
var winh=0;                       // screen height
var nln=String.fromCharCode(10);  // line break
var plrlifedef=1;                 // life amount default
var plrlife=0;                    // life counter
var plrlvl=0;                     // level
var plrlvlthr=15;                 // level threshold to speed up
var astamtdef=2;                  // asteroid amount default
var astamt=0;                     // asteroid amount in game
var asttyps=['x','m','s'];        // asteroid types
var astpool=[];                   // asteroid pool
var astlst=[];                    // active asteroids
var shppx=0;                      // ship position x
var shppy=0;                      // ship position y
var shptmr=500;                   // wait to show ship
var expaud='';                    // explosion audio
var clkaud='';                    // click audio
var btnaud='';                    // button audio



/* add class to element */
function addClassToElementById(elmid,elmcls){
  var cls=getClassStringForElementById(elmid);
  if(cls.includes(elmcls)==false){
    var newcls=cls.concat(' '+elmcls);
    document.getElementById(elmid).className=newcls;
  }
}

/* remove class from element */
function removeClassFromElementById(elmid,elmcls){
  var cls=getClassStringForElementById(elmid);
  var newcls=cls.split(elmcls).join('');
  document.getElementById(elmid).className=newcls;
}

/* get the string with class names for element */
function getClassStringForElementById(elmid){
  return document.getElementById(elmid).className;
}

/* get inner html for element by id */
function getElementInnerHtmlById(elmid){
  return document.getElementById(elmid).innerHTML;
}

/* set inner html for element by id */
function setElementInnerHtmlById(elmid,str){
  document.getElementById(elmid).innerHTML=str;
}

/* set width/height for element by id */
function setSizeForElementById(elmid,sx,sy){
  if(sx>=0){document.getElementById(elmid).style.width=parseInt(sx)+'px';}
  if(sy>=0){document.getElementById(elmid).style.height=parseInt(sy)+'px';}
}

/* set position for element by id */
function setPositonForElementById(elmid,px,py){
  if(px>=0){document.getElementById(elmid).style.left=parseInt(px)+"px";}
  if(py>=0){document.getElementById(elmid).style.top=parseInt(py)+"px";}
}

/* get x position for element by id */
function getXPositionForElementById(elmid){
  return document.getElementById(elmid).style.left;
}

/* get y position for element by id */
function getYPositionForElementById(elmid){
  return document.getElementById(elmid).style.top;
}

/* get visibility for element (class with 'show') */
function isElementVisibleById(elmid){
  return getClassStringForElementById(elmid).includes('show')==true;
}

/* hide cover layer */
function hideCover(){
  addClassToElementById('cover1','cover1hide');
  addClassToElementById('cover2','cover2hide');
}

/* format number with leading zero */
function numFormat(num,size){
  var str="000000000"+num;
  return str.substr(str.length-size);
}

/* remove item from list by key */
function removeItemFromListByKey(key,lst){
  return lst.splice(key,1);
}

/* add item to list */
function addItemToList(item,lst){
  lst.push(item);
}



/* draw asteroids */
function drawAsteroids(){
  // new html
  var html='';
  var key=0;
  astlst.forEach((data)=>{
    html+='    <svg class="rock rock-'+data[0]+'" aspect-ratio="XminYmin" style="top:'+parseInt(data[4])+'px;left:'+parseInt(data[3])+'px;transform:rotate('+parseInt(data[7])+'deg);" attr-key="'+key+'" attr-type="'+data[0]+'" attr-pts="'+data[1]+'">'+nln;
    html+='      <polygon points="'+data[2]+'"></polygon>'+nln;
    html+='    </svg>'+nln;
    key++;
  });
  // set html
  setElementInnerHtmlById('rocks',html);
}

/* move asteroids */
function moveAsteroids(){
  // walk list
  var key=0;
  astlst.forEach((data)=>{
    var px=data[3];
    var py=data[4];
    var sx=data[5];
    var sy=data[6];
    var deg=data[7];
    var rsp=data[8];
    // add speed
    px+=sx;
    py+=sy;
    // rotate
    deg+=rsp;
    // set data back to list
    data[3]=px;
    data[4]=py;
    data[7]=deg;
    astlst[key]=data;
    key++;
  });
}

/* adding asteroids depending on level */
function addNewAsteroidsByLevel(){
  var amt=1+Math.floor(plrlvl/(plrlvlthr*2));
  for(var x=1;x<=amt;x++){
    addAsteroidToList();
  }
}

/* add new random asteroid */
function addAsteroidToList(){
  // pick random asteroid by random type
  var num=Math.floor(Math.random()*asttyps.length);
  var item=getRandomAsteroidByType(asttyps[num]);
  // get data for asteroid
  var arr=generateAsteroidDataArray(item);
  // add to list
  addItemToList(arr,astlst);
}

/* generate data for asteroid */
function generateAsteroidDataArray(item){
  // get basic params
  var type=item[0];
  var pts=item[1];
  var shp=item[2];
  // set position
  var px=Math.floor(Math.random()*winw)-40;
  var py=-200;
  // set speed (randomly up/down/left/right)
  var spds=getRandomAsteroidSpeed();
  var sx=spds[0];
  var sy=spds[1];
  // rotation
  var deg=Math.random()*360;
  /* rotation speed */
  var rsp=0.5+(Math.random()*2);
  if(Math.random()<0.5){rsp*=-1;}
  // add to list
  var arr=[type,pts,shp,px,py,sx,sy,deg,rsp];
  // return data
  return arr;
}

/* setup initial level asteroid list */
function setupInitialLevelAsteroidList(){
  console.log('setup asteroid list');
  // empty list
  astlst=[];
  // create new list (1-n)
  for(x=1;x<(astamt+plrlvl);x++){
    addAsteroidToList();
  }
}

/* get random asteroid from list by type */
function getRandomAsteroidByType(type){
  // items
  var itmlst=astpool[0][type];
  // random item from list
  var num=Math.floor(Math.random()*itmlst.length);
  var item=itmlst[num];
  // return item
  return item;
}

/* get random speeds for asteroid */
function getRandomAsteroidSpeed(){
  // generate random speed (x,y)
  var sx=0;
  var sy=1+(Math.random()*3);
  if(plrlvl>=plrlvlthr){
    var val=plrlvl/plrlvlthr;
    sy+=(val/3)+(Math.random()*(val));
  }
  // slower for mobile
  if(winw<=960){
    sy*=0.75;
  }
  // return speeds
  return [sx,sy];
}

/* remove off screen asteroids */
function removeOffScreenAsteroids(){
  // remove asteroids
  for(var key in astlst){
    var data=astlst[key];
    var py=data[4];
    if(py>=(winh+50)){
      removeItemFromListByKey(key,astlst);
    }
  }
}

/* set the asteroid pool to choose from */
function setupAsteroidPool(){
  // type,points,shape
  astpool.push({
    'x':
    [
      ['x', 20,'19 31,8 54,14 64,10 71,35 87,24 94,33 104,65 91,78 104,110 96,116 69,113 46,88 43,75 34,60 35,50 19,40 13,19 28'],
      ['x', 20,'6 41,23 63,35 70,14 85,25 105,42 119,83 113,101 95,114 80,88 59,112 37,100 26,86 29,76 24,64 25,44 20,51 8,8 16'],
      ['x', 20,'27 26,24 23,25 26,10 42,16 71,22 76,6 82,45 92,31 113,68 117,94 107,100 99,83 65,108 75,113 48,93 17,60 11,21 22'],
      ['x', 20,'8 62,8 70,37 91,33 106,43 115,74 101,93 105,118 56,117 35,88 13,79 25,49 8,29 15,42 45'],
      ['x', 20,'5 44,12 76,30 91,14 114,42 115,59 99,78 120,73 87,93 110,113 76,104 65,116 43,110 39,60 53,101 29,94 19,75 12,37 23,33 8,15 16'],
      ['x', 20,'14 38,25 70,21 76,39 104,56 113,89 104,102 85,113 67,113 47,112 31,99 20,80 20,52 10,37 10,27 16,15 33'],
    ],
    'm':
    [
      ['m', 50,'10 49,8 56,23 57,20 68,48 61,58 69,74 51,75 40,82 36,74 24,50 11,42 12,33 10,8 34'],
      ['m', 50,'3 30,26 53,14 60,35 74,64 67,74 54,62 45,79 33,71 11,60 19,46 10,42 12,26 7,17 27'],
      ['m', 50,'10 35,5 49,15 59,22 73,47 79,64 74,78 58,76 39,73 14,60 8,35 9,29 14,19 9,12 11,5 17'],
      ['m', 50,'8 36,19 50,10 64,30 77,40 63,46 78,60 73,71 65,60 42,70 55,74 44,71 14,63 11,43 21,46 9,30 9,16 20'],
      ['m', 50,'8 62,33 80,63 76,76 63,78 42,78 19,52 8,24 6,8 18'],
      ['m', 50,'12 34,26 42,12 52,29 62,42 59,43 68,63 62,75 49,77 32,75 19,70 15,53 7,37 8,33 19,26 8,10 13'],
    ],
    's':
    [
      ['s',100,'5 18,6 33,19 39,35 33,39 19,20 7,12 9,5 14'],
      ['s',100,'10 17,10 27,14 31,28 35,37 24,35 17,25 9,17 9,8 14'],
      ['s',100,'2 24,14 30,16 35,26 34,33 25,29 16,24 13,18 19,16 12,15 10,8 11,6 17'],
      ['s',100,'8 11,8 20,14 31,17 36,27 35,33 33,31 17,26 13,16 8,5 9'],
      ['s',100,'4 19,4 27,18 34,26 28,31 17,30 11,25 6,12 3,6 6'],
      ['s',100,'8 16,6 28,15 31,12 35,25 32,23 26,31 26,33 13,27 4,23 3,17 20,19 7,15 7,8 13'],
    ]
  });
}



/* find a free position for ship */
function setShipPosition(){
  console.log('setup ship position');
  // stop moving
  shpspdx=0;
  shpspdy=0;
  // set position
  shppx=Math.floor(winw/2);
  shppy=winh-50;
  setTimeout(()=>{showShip()},shptmr);
}

/* change ship position by value */
function changeShipPosition(val){
  shppx+=val;
}

/* draw ship */
function drawShip(){
  // borders left/right
  if(shppx<=20){shppx=20;}
  if(shppx>=winw-40){shppx=winw-40;}
  // set position
  setPositonForElementById('plr',shppx,shppy);
  // display ship
  setElementInnerHtmlById('plr',getShipHtml());
}

/* hide ship */
function hideShip(){
  removeClassFromElementById('plr','show-elem');
}

/* show ship */
function showShip(){
  addClassToElementById('plr','show-elem');
}

function getShipHtml(){
  var html='';
  html+='  <svg class="ship" aspect-ratio="XminYmin">'+nln;
  html+='    <polygon points="17 24,27 34,17 4,8 32"></polygon>'+nln;
  html+='  </svg>'+nln;
  return html;
}



/* level up */
function addLevel(){
  plrlvl++;
}

/* draw level */
function drawLevel(){
  setElementInnerHtmlById('level',numFormat(plrlvl,5));
}



/* take one life */
function removeLife(){
  plrlife--;
}



/* check collisions */
function checkCollisions(){
  // object lists
  var asts=document.querySelectorAll('#rocks>.rock');
  var shps=document.querySelectorAll('#plr>.ship');
  // ship with asteroids
  if(isElementVisibleById('plr')){
    shps.forEach((shpobj)=>{
      asts.forEach((astobj)=>{
        var ptop=parseInt(astobj.style.top);
        if((ptop>=winh-200)&&(ptop<=winh-50)){
          if(doCollide(shpobj,astobj)){
            expaud.cloneNode(true).play();
            removeLife();
            stopGame();
            showGameInfo();
          }
        }
      });
    });
  }
  // default
  return true;
}

/* do two object collide */
function doCollide(obj1,obj2) {
  if((typeof obj1==='object')&&(typeof obj2==='object')){
    // object bounderies
    var one=obj1.getBoundingClientRect();
    var two=obj2.getBoundingClientRect();
    // check collision
    if(  one.left < two.left      + two.width-10 
      && one.left + one.width-10  > two.left 
      && one.top  < two.top       + two.height-10 
      && one.top  + one.height-10 > two.top) {
        return true;
    }
  }
  return false;
}



/* update screen */
function updateScreen(){
  // update objects
  drawAsteroids();
  drawShip();
  // update data
  drawLevel();
}



/* start game */
function startGame(){
  console.log('start game');
  // if there are lives left
  if(plrlife>0){
    removeClassFromElementById('start','show-elem');
    setupInitialLevelAsteroidList();
    run=true;
    setTimeout(()=>{setShipPosition()},250);
  }
}

/* stop game */
function stopGame(){
  console.log('stop game');
  run=false;
  hideShip();
}

/* reset game */
function resetGame(){
  console.log('reset game');
  // clear asteroids
  astlst=[];
  // reset asteroid amount
  astamt=astamtdef;
  // reset player
  plrlife=plrlifedef;
  plrlvl=0;
  // reset ship
  shpspdx=0;
  shpspdy=0;
  // update everything
  updateScreen();
  // no pause
  brk=false;
}

/* setup game */
function setupGame(){
  console.log('setup game');
  // the pool to choose from
  setupAsteroidPool();
  // setup game audio
  setupAudio();
  // different amounts for different screens
  if(winw<1024){astamtdef=1;}
  if(winw>1600){astamtdef=3;}
  // setup first asteroids
  astamt=astamtdef;
  setupInitialLevelAsteroidList();
}

/* setup game audio files */
function setupAudio(){
  expaud=new Audio('explosion.wav');
  expaud.volume=0.5;
  clkaud=new Audio('click.wav');
  clkaud.volume=0.5;
  btnaud=new Audio('button.wav');
  btnaud.volume=0.5;
}

/* show game info */
function showGameInfo(){
  removeClassFromElementById('start','show-elem');
  removeClassFromElementById('gameover','show-elem');
  var elmid='start';
  if(plrlife==0){elmid='gameover';}
  setTimeout(()=>{addClassToElementById(elmid,'show-elem')},250);
}



/* init game */
function init(){
  console.log('init');
  // setup basics
  winw=window.innerWidth;
  winh=window.innerHeight;
  // set game content
  setupGame();
  // reset game params
  resetGame();
  // show gameinfo
  showGameInfo();
  // and go
  console.log('ready!');
}



// intervall for moving objects
let movr=setInterval(()=>{
  // if game is not paused
  if(brk==false&&run==true){
    moveAsteroids();
    checkCollisions();
    updateScreen();
  }
},50);

// intervall to remove off screen asteroids
let cln=setInterval(()=>{
  // if game is not paused
  if(brk==false&&run==true){
    removeOffScreenAsteroids();
  }
},2000);

/* count up level (time) */
let levl=setInterval(()=>{
  // if game is not paused
  if(brk==false&&run==true){
    addLevel();
    clkaud.cloneNode(true).play();
    if(plrlvl%2==0){addNewAsteroidsByLevel();}
  }
},1000);



/* listener for key press */
window.addEventListener('keydown',(event)=>{
  switch(event.which){
    // s(tart)
    case 83:
      if(run==false){
        btnaud.cloneNode(true).play();
        startGame();
      }
      break;
    // r(eset)
    case 82:
      btnaud.cloneNode(true).play();
      stopGame();
      resetGame();
      showGameInfo();
      break;
    // p(ause)
    case 80:
      btnaud.cloneNode(true).play();
      brk=!brk;
      break;
    // left (left/a)
    case 65:
    case 37:
      changeShipPosition(-10);
      break;
    // right (right/d)
    case 68:
    case 39:
      changeShipPosition(10);
      break;
    }
},false);

/* listener for mouse click */
window.addEventListener('click',(event)=>{
  event.preventDefault();
  if(run==false){
    if(isElementVisibleById('start')){
      window.dispatchEvent(new KeyboardEvent('keydown',{'keyCode':83,'which':83}));
    }
    if(isElementVisibleById('gameover')){
      window.dispatchEvent(new KeyboardEvent('keydown',{'keyCode':82,'which':82}));
    }
  }
},false);

/* lister for scroll wheel */
window.addEventListener("wheel",(event)=>{
  event.preventDefault();
  shppx-=Math.floor(event.deltaY*2);
},false);

/* listener for touch on mobiles */
window.addEventListener("touchmove",(event)=>{
  event.preventDefault();
  shppx=Math.floor(event.changedTouches[0].pageX);
},false);



/* listener for window resize */
window.addEventListener('resize',()=>{
  stopGame();
  init();
},false);

/* wait until document is fully loaded and ready to start */
let stateCheck=setInterval(()=>{
  if(document.readyState=='complete'){
    clearInterval(stateCheck);
    init();
    hideCover();
  }
},250);